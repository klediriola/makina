from django.urls import path

from vehicles.views import get_vehicles, get_vehicle_details

urlpatterns = [
    path("vehicles", get_vehicles, name="vehicle_list"),
    path("vehicles/<int:pk>", get_vehicle_details, name="vehicle_detail")
]
