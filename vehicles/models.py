from django.db import models

# Create your models here.
class Vehicle(models.Model):
    AUTOMATIC = "automatic"
    MANUAL = "manual"
    TRANSMISSION_CHOICES = [
        (AUTOMATIC, "Automatic"),
        (MANUAL, "Manual"),
    ]

    # brand varchar(100) not null
    brand = models.CharField(max_length=100, null=False)
    # brand varchar(100) not null
    model = models.CharField(max_length= 100, null= False)
    # year int not null constraint check_positive check (year>0)
    year= models.PositiveIntegerField(null=False)
    transmission = models.CharField(max_length=32, null=True, choices=TRANSMISSION_CHOICES)
    photo = models.ImageField(null=True, upload_to="vehicles")

    def __str__(self):
        return f'{self.brand} {self.model}'




