from django.shortcuts import render

from vehicles.models import Vehicle

# 127.0.0.1:8000/vehicles
# Create your views here.
def get_vehicles(request):
    #select all fromvehicles-vehicle
    vehicles = Vehicle.objects.all()
    return render(request, "vehicle_list.html", context={
       "vehicles" : vehicles
       
       })


# 127.0.0.1:8000/vehicles/3
def get_vehicle_details(request, pk):
    #select all from vehicles_vehiclke where id =%s
    vehicle = Vehicle.objects.get(pk=pk)
    return render(request, "vehicle_detail.html", context={
        "vehicle": vehicle
    })